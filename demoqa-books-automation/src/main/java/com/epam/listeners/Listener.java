package com.epam.listeners;

import com.epam.utilities.webDriversUtility.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class Listener implements ITestListener {
    private static final Logger logger = LogManager.getLogger(Listener.class.getName());
    public void onTestStart(ITestResult result) {
        logger.info("The " + result.getMethod().getMethodName() + " has started");
    }


public  void onTestSuccess(ITestResult result) {
        logger.info("The test has successfully completed with the status: " + result.getStatus());
    }

   public void onTestFailure(ITestResult result) {
       try {
           File failedScreen = ((TakesScreenshot) WebDriverManager.setUpDriver("chrome")).getScreenshotAs(OutputType.FILE);
           FileUtils.copyFile(failedScreen, new File("//target/screenshots/" + getCurrentTimeStamp() + ".png"));
       } catch (Exception exception) {
           logger.error("Failed to capture the Screen shot");
       }
   }

    public void onTestSkipped(ITestResult result) {
        logger.info("The " + result.getMethod().getMethodName()+ " has skipped");
    }



  public void onFinish(ITestContext context) {
      logger.info("The Test execution is finished  " );

  }

  private String getCurrentTimeStamp(){
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu_MM_dd_HH_mm_ss");
        return ZonedDateTime.now().format(formatter);

  }
}
