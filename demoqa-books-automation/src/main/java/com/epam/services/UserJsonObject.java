package com.epam.services;

import org.json.JSONObject;

public class UserJsonObject {
    public static JSONObject jsonObject(String userName,String userPassword) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName", userName);
        jsonObject.put("password", userPassword);
        return jsonObject;
    }
}
