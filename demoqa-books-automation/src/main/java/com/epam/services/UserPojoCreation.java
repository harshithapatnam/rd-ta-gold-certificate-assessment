package com.epam.services;

import com.epam.pojos.UserPojo;

public class UserPojoCreation {
   private UserPojoCreation(){

    }
    public static UserPojo getUserPojo(String userName,String userPassword){
       UserPojo userPojo = new UserPojo();
       userPojo.setUserName(userName);
       userPojo.setUserPassword(userPassword);
       return userPojo;
    }
}
