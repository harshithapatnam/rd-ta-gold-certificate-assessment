package com.epam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    protected WebDriver webDriver;

    @FindBy(css = "input[id='userName']")
    private WebElement userNameField;
    @FindBy(css = "input[id='password']")
    private WebElement userPasswordField;
    @FindBy(css = "button[id='login']")
    private WebElement loginButton;
    @FindBy(css = "p[id='name']")
    private WebElement errorMessage;
    public HomePage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver,this);
    }
    public HomePage openLoginHomePage(){
        webDriver.get("https://demoqa.com/login");
        return this;
    }

    public HomePage enterUserCredentials(String userName,String userPassword){
        Actions actions = new Actions(webDriver);
        actions.scrollByAmount(0,50).moveToElement(userNameField).click().sendKeys(userName).build().perform();
        actions.scrollByAmount(0,200).moveToElement(userPasswordField).click().sendKeys(userPassword).build().perform();
        return this;
    }
    public ProfilePage doLogin(){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(loginButton).click(loginButton).build().perform();
        return new ProfilePage(webDriver);
    }
    public void getUrl(){
        System.out.println(webDriver.getCurrentUrl());
    }
    public String getErrorMessage(){
        return errorMessage.getText();
    }
}
