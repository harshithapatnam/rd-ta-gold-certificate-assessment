package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookDetailsPage {
    protected WebDriver webDriver;

    @FindBy(xpath = "//div[@id='author-wrapper']//label[@id='userName-value']")
    private WebElement authorName;

    @FindBy(xpath = "//div[@id='publisher-wrapper']//label[@id='userName-value']")
    private WebElement publisherName;

    public BookDetailsPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver,this);
    }


public String getAuthor(){
        new Actions(webDriver).scrollByAmount(0,100).build().perform();
    return authorName.getText();

}

public String getPublisher(){
    new Actions(webDriver).scrollByAmount(0,100).build().perform();
        return publisherName.getText();
}
}
