package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ProfilePage {
    protected WebDriver webDriver;

    @FindBy(css = "label[id='userName-value']")
    private WebElement userNameDisplayed;

    public ProfilePage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver,this);
    }
    public String getUserNameFromProfile(){
        waitToClickElement(userNameDisplayed);
        return userNameDisplayed.getText();
    }

    public void waitToClickElement(WebElement webElement){
        new WebDriverWait(webDriver, Duration.ofSeconds(20)).until(ExpectedConditions.visibilityOf(webElement));
    }
}
