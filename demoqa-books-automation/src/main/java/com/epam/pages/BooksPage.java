package com.epam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BooksPage {
    protected WebDriver webDriver;



    public BooksPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver,this);
    }
    public BooksPage openBooksPage(){
        webDriver.get("https://demoqa.com/books");
        return this;
    }

    public BookDetailsPage getBookDetails(String bookName){
        WebElement bookPageUrl = webDriver.findElement(By.xpath("//a[text()='"+bookName+"']"));
         new Actions(webDriver).scrollByAmount(0,100).moveToElement(bookPageUrl).click().build().perform();

        webDriver.getCurrentUrl();
        return new BookDetailsPage(webDriver);
    }
}
