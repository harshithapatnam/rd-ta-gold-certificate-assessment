package com.epam.pojos;

import java.util.ArrayList;
import java.util.Date;

public class BookResponsePojo {



    public class Book{
        public String isbn;
        public String title;
        public String subTitle;
        public String author;
        public Date publish_date;
        public String publisher;
        public int pages;
        public String description;
        public String website;
    }

    public class Root{
        public ArrayList<Book> books;
    }


}
