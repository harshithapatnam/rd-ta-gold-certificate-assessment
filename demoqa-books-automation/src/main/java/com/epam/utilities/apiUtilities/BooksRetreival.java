package com.epam.utilities.apiUtilities;

import com.epam.constants.UriConstants;
import com.epam.pages.ProfilePage;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.Properties;

import static io.restassured.RestAssured.given;

public class BooksRetreival {
    private BooksRetreival(){

    }
    private static final Logger logger = LogManager.getLogger(BooksRetreival.class.getName());
    static String userCreationPath;
    public static void setBooksPath(String user_creation_Path){
        userCreationPath = user_creation_Path;
    }
    public  static void setBaseURI(){
        RestAssured.baseURI = UriConstants.BASE_URI;
    }


    public static Response getBooks(){
        setBaseURI();
       return given().request(Method.GET,userCreationPath).prettyPeek().then().extract().response();

    }
}
