package com.epam.utilities.apiUtilities;

import com.epam.constants.UriConstants;
import com.epam.pojos.UserPojo;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class UserCreationUtility {
    private UserCreationUtility(){

    }
    private static final Logger logger = LogManager.getLogger(UserCreationUtility.class.getName());
    static String userCreationPath;
    public static void setUserCreationPath(String user_creation_Path){
        userCreationPath = user_creation_Path;
    }
    public  static void setBaseURI(){
        RestAssured.baseURI = UriConstants.BASE_URI;
    }


    public static Response createUser(JSONObject jsonObject, String httpMethod){
        setBaseURI();
        if(httpMethod.equals("POST")){
            return given().body(jsonObject.toString())
                    .header("Content-Type","application/json").
                    request(Method.POST,"/Account/v1/User")
                    .then()
                    .extract().response();



        }
        else{
            logger.error("The user account can be created using Post method only");
            return null;
        }

    }

    public static int getStatusCode(Response response){
        return response.getStatusCode();
    }
    public static String getCreatedUserName(Response response){
        return response.jsonPath().getString("username");
    }
    public static String getErrorMessage(Response response){
        return response.jsonPath().getString("message");
    }

}
