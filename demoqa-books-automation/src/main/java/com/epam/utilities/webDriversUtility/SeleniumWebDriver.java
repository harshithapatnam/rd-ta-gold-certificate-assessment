package com.epam.utilities.webDriversUtility;

import org.openqa.selenium.WebDriver;

public interface SeleniumWebDriver {
    public WebDriver getWebDriver();

}
