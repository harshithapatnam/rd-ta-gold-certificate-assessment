package com.epam.utilities.webDriversUtility.driverImplementations;

import com.epam.utilities.webDriversUtility.SeleniumWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeWebDriver implements SeleniumWebDriver {

    public WebDriver getWebDriver() {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--remote-allow-origins=*");
            chromeOptions.addArguments("--start-maximized");
            return new ChromeDriver(chromeOptions);
        }

}
