package com.epam.utilities.webDriversUtility.driverImplementations;

import com.epam.utilities.webDriversUtility.SeleniumWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxWebDriver implements SeleniumWebDriver {
    public WebDriver getWebDriver() {
        return new FirefoxDriver();
    }
}
