package com.epam.utilities.webDriversUtility;

import org.openqa.selenium.WebDriver;

public class WebDriverManager {
    private static WebDriver WEBDRIVER;
    private WebDriverManager(){

    }

    public static WebDriver setUpDriver(String driverType){
        if(WEBDRIVER == null)
            WEBDRIVER =  WebDriversFactory.getDriver(driverType).getWebDriver();
        return WEBDRIVER;
    }
    public static void closeDriver(){
        WEBDRIVER.quit();
        WEBDRIVER = null;
    }




}
