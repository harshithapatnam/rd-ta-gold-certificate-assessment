package com.epam.utilities.webDriversUtility.driverImplementations;

import com.epam.utilities.webDriversUtility.SeleniumWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class EdgeWebDriver implements SeleniumWebDriver {
    public WebDriver getWebDriver() {
        return new EdgeDriver();
    }
}
