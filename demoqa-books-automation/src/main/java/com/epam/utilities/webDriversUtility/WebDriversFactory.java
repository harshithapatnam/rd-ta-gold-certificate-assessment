package com.epam.utilities.webDriversUtility;

import com.epam.utilities.webDriversUtility.driverImplementations.ChromeWebDriver;
import com.epam.utilities.webDriversUtility.driverImplementations.EdgeWebDriver;
import com.epam.utilities.webDriversUtility.driverImplementations.FirefoxWebDriver;

public class WebDriversFactory {
    public static SeleniumWebDriver getDriver(String driverType){
        switch(driverType.toLowerCase()){
            case "chrome": return new ChromeWebDriver();
            case "edge": return new EdgeWebDriver();
            case "firefox": return new FirefoxWebDriver();
            default: return null;
        }
    }
}
