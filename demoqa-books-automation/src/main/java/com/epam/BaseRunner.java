package com.epam;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
              glue= {"com.epam"},
                features={"src/test/resources/features","src/test/resources/uifeatures"})

public class BaseRunner extends AbstractTestNGCucumberTests {
}
