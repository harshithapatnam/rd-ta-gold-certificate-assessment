package com.epam.apiStepDefintions;

import com.epam.pages.BookDetailsPage;
import com.epam.pages.BooksPage;
import com.epam.pojos.BookResponsePojo;
import com.epam.utilities.apiUtilities.BooksRetreival;
import com.epam.utilities.webDriversUtility.WebDriverManager;
import io.cucumber.java.AfterAll;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class BooksReterivalStepDefinitions {

    Response response;
    int numberOfBooks;
    BookResponsePojo bookPojo;
    BooksPage booksPage ;
    BookDetailsPage bookDetailsPage;
    @Given("the api call is made to {string}")
    public void theApiCallIsmadeTo(String urlPath) {
       BooksRetreival.setBooksPath(urlPath);
    }

    @When("the get call is made")
    public void theGetCallIsMade() {
        response = BooksRetreival.getBooks();
    }

    @Then("the status code must be {int}")
    public void theStatusCodeMustBe(int expectedStatusCode) {
        Assert.assertEquals(response.getStatusCode(),200);
    }





    @Then("the list of books must returned must be matched with that on ui")
    public void theListOfBooksMustReturnedMustBeMatchedWithThatOnUi() {
        booksPage = new BooksPage(WebDriverManager.setUpDriver("chrome")).openBooksPage();
        List<HashMap> books = response.jsonPath().getList("books");
        for(int i=0 ;i<books.size();i++){
           String bookName = books.get(i).get("title").toString();
            verifyBookDetails(books, i, bookName);
            new Actions(WebDriverManager.setUpDriver("chrome")).scrollByAmount(0,50);

        }

    }

    private void verifyBookDetails(List<HashMap> books, int i, String bookName) {

        bookDetailsPage = booksPage.getBookDetails(bookName);
        String bookAuthor = books.get(i).get("author").toString();
        Assert.assertEquals(bookAuthor,bookDetailsPage.getAuthor());
        String bookPublisher = books.get(i).get("publisher").toString();
        Assert.assertEquals(bookPublisher,bookDetailsPage.getPublisher());
    }


}
