package com.epam.apiStepDefintions;

import com.epam.pojos.UserPojo;
import com.epam.services.UserJsonObject;
import com.epam.services.UserPojoCreation;
import com.epam.utilities.apiUtilities.UserCreationUtility;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;

public class UserCreationStepDefinitions {
//    public ApiCommonSteps commonSteps;
//    UserCreationStepDefinitions(ApiCommonSteps commonSteps){
//        this.commonSteps = commonSteps;
//
//    }
    Response response;
    @Given("the path of api call to create the user is {string}")
    public void thePathOfApiCallToCreateTheUserIs(String userCreationPath) {
        UserCreationUtility.setUserCreationPath(userCreationPath);
    }

    @When("the user with {string} and {string} sent through {string} method")
    public void theUserWithAndSentThroughMethod(String userName, String userPassword, String httpMethod) {
        createUser(userName, userPassword, httpMethod);

    }

    private void createUser(String userName, String userPassword, String httpMethod) {
        JSONObject userJsonObject = UserJsonObject.jsonObject(userName,userPassword);
        response =  UserCreationUtility.createUser(userJsonObject, httpMethod);

    }

    @Then("the api call must return status code {int}")
    public void theApiCallMustReturnStatusCode(int expectedStatusCode) {
        Assert.assertEquals(UserCreationUtility.getStatusCode(response),expectedStatusCode);
    }

    @And("the user account must be created with the {string}")
    public void theUserAccountMustBeCreatedWithThe(String expectedUserName) {
        Assert.assertEquals(UserCreationUtility.getCreatedUserName(response),expectedUserName);
    }
    @When("the user with existing {string} and {string} sent through {string} method")
    public void theUserWithExistingCredentialsAndSentThroughMethod(String userName, String userPassword, String httpMethod) {
        createUser( userName, userPassword, httpMethod);
    }

    @When("the user with invalid {string} or {string} sent through {string} method")
    public void theUserWithInvalidCredentialsAndSentThroughMethod(String userName, String userPassword, String httpMethod) {
        createUser( userName, userPassword, httpMethod);
    }

    @And("the error message displayed must be {string}")
    public void theErrorMessageDisplayedMustBe(String expectedErrorMessage) {
        Assert.assertEquals(UserCreationUtility.getErrorMessage(response),expectedErrorMessage);
    }
}
