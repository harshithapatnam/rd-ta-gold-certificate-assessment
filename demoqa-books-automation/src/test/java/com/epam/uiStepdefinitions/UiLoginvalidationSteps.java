package com.epam.uiStepdefinitions;

import com.epam.pages.HomePage;
import com.epam.pages.ProfilePage;
import com.epam.utilities.webDriversUtility.WebDriverManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class UiLoginvalidationSteps {
    HomePage homePage;
    ProfilePage profilePage;


    @Given("The login page is open")
    public void theLoginPageIsOpen() {
        homePage = new HomePage(WebDriverManager.setUpDriver("chrome"));
        homePage.openLoginHomePage();
    }

    @When("the user enter {string} and {string}")
    public void theUserEnterAnd(String userName, String userPassword) {
       homePage.enterUserCredentials(userName,userPassword);


    }

    @And("click login")
    public void clickLogin() {
      profilePage = homePage.doLogin();
    }

    @Then("the {string} must be displayed")
    public void theMustBeDisplayed(String userName) {
       Assert.assertEquals(profilePage.getUserNameFromProfile(),userName);
    }

    @Then("the {string} message must be displayed")
    public void theMessageMustBeDisplayed(String expectedErrorMessage) {
        Assert.assertEquals(homePage.getErrorMessage(),expectedErrorMessage);
    }
}
