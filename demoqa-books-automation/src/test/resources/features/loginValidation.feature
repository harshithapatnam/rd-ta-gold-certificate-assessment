Feature: This feature is to verify the validation of login to the Book store web app using api
  Scenario Outline: This scenario is to verify the user account created
    Given the path of api call to create the user is "Account/v1/User"
    When the user with "<userName>" and "<userPassword>" sent through "POST" method
    Then the api call must return status code 201
    And the user account must be created with the "<userName>"
    Examples:
      | userName     | userPassword |
      | Mishti rajvansh | Mishti@1232  |

  @existingUserCredentials @invalid
  Scenario Outline: This scenario is to verify the user account whose credentials are already existing
    Given the path of api call to create the user is "Account/v1/User"
    When the user with existing "<userName>" and "<userPassword>" sent through "POST" method
    Then the api call must return status code 406
    And the error message displayed must be "User exists!"
    Examples:
      | userName     | userPassword |
      | Mishti rajvansh | Mishti@1232  |

  @invalidPassword @invalid
  Scenario Outline: This scenario is to verify the user account creation with invalid password
    Given the path of api call to create the user is "Account/v1/User"
    When the user with invalid "<userName>" or "<userPassword>" sent through "POST" method
    Then the api call must return status code 400
    And the error message displayed must be "Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer."
    Examples:
      | userName      | userPassword     |
      | Mishti Agarwal| mishti@agarwal   |
      | Sourya Deep   | sourya#Deep      |
      | Sai Harshitha | Harshitha123     |
      |  Harshitha | harshitha@123     |
      | Kaveri     | Kavs#1             |
      | Vikram     | VIKRAM#123         |
