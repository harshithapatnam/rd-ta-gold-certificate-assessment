Feature: This feature is to check the validity of login for a created user
  Scenario Outline: This scenario is to verify the validation of the user
    Given The login page is open
    When the user enter "<userName>" and "<userPassword>"
    And click login
    Then the "<userName>" must be displayed
    Examples:
      | userName | userPassword |
      | Mishti rajvansh | Mishti@1232  |

  Scenario Outline: This scenario is to verify the validation of the invalid user
    Given The login page is open
    When the user enter "<userName>" and "<userPassword>"
    And click login
    Then the "Invalid username or password" message must be displayed
    Examples:
      | userName | userPassword |
      | Vikram     | Vikram#123 |
